p2pConnection
=============

EE4210 Project, create for P2P Connection.

Group Members:
- Song Yangyu - A0077863N
- Hu Yuxin    - A0078677H

## How to run

You'll need to use Eclipse to run it, and make sure you have internet access on the maching that you're running.

Firstly, import this project into eclipse.

Then, you can either test it on a single machine, or on multiple machines.

If you want to test on a single machine, run the main method at `src/p2pConnection/runner/IntegratedRun.java`.

If you want to test on multiple machine, follow the following steps:

1. Goto <http://songyy.me:8082/nodes.php>, fill in the total number of running devices, and click on "start"
2. On each of the machine, find a proper device name. If you look at the folder `conf/devices/`, the configuration if different devices are saved there. Open `src/p2pConnection/runner/IndividualRun.java`, change the device name to be one of the devices file in the folder above, and run the main method. Please make sure that there're no duplicate device names chosen.
3. In the case that more device are declared than the actually running ones, for example, you declared there're 3 running devices, but you only started device 2&3, a different timeout would be applied onto different nodes. After the timeout, the devices (whatever registered) would start running anyway.

The final result would be output onto the console; and the intermdeiate results are saved in the folder `docs/results`

## Settings you can change

In the file `src/p2pConnection/Consts.java`, there're 2 variables you can change. `isToMinimizePeek` and `deviceFolder`.
`isToMinimizePeek` would decide the optimization goal: if you set it to true, the goal is to minimize the Peek/Average Ratio; if you set it to false, the gaol is to minimize the total Variance.

`deviceFolder` would decide the device folder that is used for storing the device configuration files. The `configFolder + "devices/";` folder is the dataset given, and another folder, `configFolder + "deviceTest/"` is the one we used for testing.

By changing these settings, you can get the result output in the corresponding files.