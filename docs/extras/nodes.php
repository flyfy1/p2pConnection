<?php
require_once 'php-activerecord/ActiveRecord.php';

define('START_TIME_NAME','start_time');
define('SQL_DATE_FORMAT','Y-m-d H:i:s');

function keys_exists($keys,$arr){
  foreach($keys as $key){
    if(!array_key_exists($key,$arr))  return false;
  }
  return true;
}

function get_nodes_str($device_num, $nodes){
  $devices_json = "[";
  $first = true;
  foreach($nodes as $node){
    if($first){
      $first = false;
    } else{
      $devices_json .= ",";
    }
    $devices_json .= $node->to_json();
  }
  $devices_json .= "]";
  return "{\"device_num\":$device_num,\"devices\":$devices_json}";
}

ActiveRecord\Config::initialize(function($cfg)
{
    $cfg->set_model_directory('.');
    $cfg->set_connections(array(
        'development' => 'mysql://ee4218:pF7h2XNBzDw6tzbL@localhost/ee4218'));
});

class Node extends ActiveRecord\Model{}
class Cst extends ActiveRecord\Model{}


@$action = $_POST['action'];
if(empty($action)){
  // Display a webform for posting the request
?>
<html>
<body>
<form method='POST'>
<p>Total Device: </p> <input name='device_num' type='text'></input>
<input type='submit' name='action' value='start'/>
<input type='submit' name='action' value='done'/>
</form>
</body>
</html>
<?php
  exit();
}
unset($_POST['action']);

switch($action){
case 'update':
  $required_keys = array('name','ip','port');
  if(!keys_exists($required_keys,$_POST)){
    echo "required keys: ";
    array_map(function($a){ echo $a.", "; },$required_keys);
    exit();
  }

  // Try to find node via name first
  $_POST['update_time'] = date(SQL_DATE_FORMAT);
  $name = isset($_POST['name']) ? $_POST['name'] : NULL;

  if($name){
    $node = Node::find_by_name($name);

    if(empty($node)){
      $node = Node::create($_POST);
    } else{
      $node->update_attributes($_POST);
    }
  }

  echo $node->id;
  break;

case 'start':
  $node = Cst::find_by_key(START_TIME_NAME);
  if(empty($node)){
    $node = Cst::create(array('key'=>START_TIME_NAME,'value'=>time()));
  } else{
    $node->update_attributes(array('value'=>time()));
  }

  if(!array_key_exists('device_num',$_POST)){
    echo "Device Number Needed.";
    exit();
  }

  $deviceNumAttr = array('key'=>'device_num','value'=>$_POST['device_num']);

  $deviceNumRecord = Cst::find_by_key('device_num');
  if(empty($deviceNumRecord)){
    $deviceNumRecord = Cst::create($deviceNumAttr);
  } else{
    $deviceNumRecord->update_attributes($deviceNumAttr);
  }

  echo "start_time set at: $node->value";
  break;

case 'query':
  $result = Cst::find_by_key(START_TIME_NAME);
  if(empty($result)){
    echo "Start not set yet.";
    exit();
  }

  // update device number
  $deviceNumRecord = Cst::find_by_key('device_num');
  $deviceNum = (int)$deviceNumRecord->value;;
  
  // Find record whcih are set later than the current time.
  $start_time = (int)$result->value;
  $start_time_for_sql = date(SQL_DATE_FORMAT,$start_time - 20);
  $found_nodes = Node::all(array('conditions' => array('update_time > ?', $start_time_for_sql)));

  echo get_nodes_str($deviceNum, $found_nodes);
  break;

case 'done':
  // Clear Records
  $record = Cst::find_by_key(START_TIME_NAME);
  if(!empty($record)) $record->delete();
  echo "Done.";
  break;
case 'test_list':
  $found_nodes = Node::all();
  echo get_nodes_str($found_nodes);
  break;
default:
  echo "Undefined Action: $action";
}
