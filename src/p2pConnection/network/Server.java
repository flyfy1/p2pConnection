package p2pConnection.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread{
	ServerSocket socket;
	public static interface OnClientActionListener{
		public void onClientAction(String clientRequest, DataOutputStream outToClient) throws IOException;
	}
	
	OnClientActionListener listener;
	
	private Server(OnClientActionListener listener){
		this.listener = listener;
	}
	
	public static Server getServer(int portNumber, OnClientActionListener listener){
		if(listener == null) return null;
		
		Server s = new Server(listener);
		try {
			s.socket = new ServerSocket(portNumber);
		} catch (IOException e) {
			return null;
		}
		
		return s;
	}
	
	@Override
	public void run(){
		while(true){
			try {
				Socket connectionSocket = socket.accept();
				
				BufferedReader inFromClient = new BufferedReader(
						new InputStreamReader(
							connectionSocket.getInputStream()));
				DataOutputStream outToClient =
						new DataOutputStream(connectionSocket.getOutputStream());
				
				String line = inFromClient.readLine();
				listener.onClientAction(line, outToClient);
				
				outToClient.flush();
				connectionSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally{
				
			}
		}
	}
}
