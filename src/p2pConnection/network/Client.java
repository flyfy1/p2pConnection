package p2pConnection.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	public static String sendCommand(String ipAddr, int portNum, String msgToSend) throws UnknownHostException, IOException{
        Socket clientSocket = new Socket(ipAddr, portNum);
        DataOutputStream outToServer = new DataOutputStream(
                clientSocket.getOutputStream());
        BufferedReader inFromServer = 
                new BufferedReader(new InputStreamReader(
                    clientSocket.getInputStream()));
        
        outToServer.writeBytes(msgToSend + '\n');
        
        String line =inFromServer.readLine();
        if(line == null) throw new IOException();
        
        clientSocket.close();
		return line.toString();
	}
}