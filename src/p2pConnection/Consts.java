package p2pConnection;

public class Consts {
	public static final String configFolder = "conf/";
	public static final String csvFolder = "docs/results/";
	
	public static final String INFO_SERVER = "http://songyy.me:8082/nodes.php";
	
	public static boolean isTesting = false;
	public static final double epsilon = 0.000001;
	public static long timeoutSec = 60;	// Waiting time: 1 minutes
	public static final String systemProfileName = "system_profile";
	public static final int serverStartingPort = 9001;

	public static final boolean isToMinimizePeek = false;	// Optimalize the Variance
	public static final String deviceFolder = configFolder + "devices/";
//	public static final String deviceFolder = configFolder + "deviceTest/";
}
