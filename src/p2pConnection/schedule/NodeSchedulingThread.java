package p2pConnection.schedule;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import p2pConnection.network.Client;
import p2pConnection.node.DeviceInfo;
import p2pConnection.node.Home;
import p2pConnection.runner.SingleHomeRunner.Callback;

/**
 * This thread would do the following jobs, in order:
 * 	
 * 	1. Ask around, to get others cost
 *  2. Check if others have changed. If no change, just pass. 
 *     Otherwise, run scheduling algorithm to get an optimal
 *  3. Pass to the next
 */
public class NodeSchedulingThread extends Thread{
	Home home;
	Map<Integer, ArrayList<DeviceInfo> > map;
	int nochangeAccumulator;
	Callback callback;
	
	public NodeSchedulingThread(Home h, int nochangeAccumulator, Callback callback) {
		this.home = h;
		this.nochangeAccumulator = nochangeAccumulator;
		this.callback = callback;
	}
	
	@Override
	public void run(){
		home.setSchedulingHasStarted();
		
		double cumulatedProfile[] = new double[24];
		
		while(home.getDeviceInfos() == null){	// Wait, in the case that there's no device info got yet
			try{TimeUnit.MICROSECONDS.sleep(100);}catch(InterruptedException e){}
		}
		
		for(DeviceInfo di:home.getDeviceInfos()){
			try{
				String gotFromServer = Client.sendCommand(di.ip, di.port, "query");
				accumulateProfile(cumulatedProfile, gotFromServer);
				di.previousProfile = gotFromServer;
				di.isLost = false;
			} catch(IOException e){
				di.isLost = true;
				if(di.previousProfile != null) accumulateProfile(cumulatedProfile, di.previousProfile);;
			}
		}
		
		boolean myProfileHasChanged = false;
		if(this.nochangeAccumulator > home.getDeviceInfos().size() * 2){
			return;
		} else if(this.nochangeAccumulator > home.getDeviceInfos().size()){
			callback.done(cumulatedProfile, home);
			// Continue running from this point on, because other nodes also needs to be notified of "done"
		} else{
			 myProfileHasChanged = Algorithm.scheduleProfile(home, cumulatedProfile);
		}
		
		if(myProfileHasChanged){
			this.nochangeAccumulator = 0;
		} else{
			this.nochangeAccumulator++;
		}
		
		// Pass to the next device
		DeviceInfo nextDevice = null;
		while(true){
			if(nextDevice == null) nextDevice = getDeviceInfoOfNextHome(home.myId);
			else nextDevice = getDeviceInfoOfNextHome(nextDevice.id);
			 
			if(nextDevice == null) break;	// No more device to schedule, it's the only one left
			try{
				tellDeviceToStartWithAccumulator(nextDevice);
				break;
			}catch(IOException e){
				// next device is not available
				nextDevice.isLost = true;
			}
		}
	}

	private void tellDeviceToStartWithAccumulator(DeviceInfo nextDevice) throws UnknownHostException, IOException {
		String gotFromServer = Client.sendCommand(nextDevice.ip, nextDevice.port, "start," + this.nochangeAccumulator);
	}

	/**
	 * In the case that null is returned, there's only one node left
	 * @param currentId
	 * @return
	 */
	private DeviceInfo getDeviceInfoOfNextHome(long currentId) {
		List<DeviceInfo> deviceInfos = home.getDeviceInfos();
		
		boolean pickThis = false;
		
		for(DeviceInfo di:deviceInfos){
			if(pickThis) return di;
			if(currentId == di.id) pickThis = true;
		}
		
		if(pickThis) return deviceInfos.get(0);
		else return null;
	}
	
	private void accumulateProfile(double[] cumulatedProfile, String profileFromServerToAccumulate) {
		String profileStrings[] = profileFromServerToAccumulate.split("\t");
		for(int i = 0 ; i < profileStrings.length ;i++){
			cumulatedProfile[i] += Double.parseDouble(profileStrings[i]);
		}
	}
}
