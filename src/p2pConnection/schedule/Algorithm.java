package p2pConnection.schedule;

import java.util.ArrayList;
import java.util.Arrays;

import p2pConnection.Consts;
import p2pConnection.node.FlexibleDevice;
import p2pConnection.node.Home;
import p2pConnection.runner.RunnerHelper;

public class Algorithm {

	private static class FinalResultHolder {
		int config[];
		double res;

		public FinalResultHolder(double initRes) {
			this.res = initRes;
		}
	}

	public static boolean scheduleProfile(Home home, double[] profile) {
		RunnerHelper.saveIntermediateHomeProfile("Before Scheduling: ", home);
		RunnerHelper.saveIntermediateSystemProfile("Before Scheduling: ",home, profile);
		
		ArrayList<FlexibleDevice> flexibleDevices = home.flexibleDevices;
		if (home.flexibleDevices == null || home.flexibleDevices.size() == 0)
			return false;

		double homeFlexibleDeviceProfile[] = home.getHomeProfile(true);
		double oldResult = calculateResultBasedOnOptimizationGoal(profile);

		double profileCopy[] = Arrays.copyOf(profile, profile.length);
		for (int i = 0; i < homeFlexibleDeviceProfile.length; i++) {
			profileCopy[i] -= homeFlexibleDeviceProfile[i];
		}

		// permutate through all possible combinations of the flexible devices
		ArrayList<ArrayList<Integer>> possiblePositionsOfDevices = new ArrayList<>();
		for (FlexibleDevice fd : flexibleDevices) {
			ArrayList<Integer> possiblePositionOfThisDevice = new ArrayList<>();
			for (int i = 0; i < fd.constraints.length; i++) {
				if (fd.isValidStartingTime(i)) {
					possiblePositionOfThisDevice.add(i);
				}
			}
			possiblePositionsOfDevices.add(possiblePositionOfThisDevice);
		}

		FinalResultHolder finalResultHolder = new FinalResultHolder(oldResult);
		scheduleProfileHelper(possiblePositionsOfDevices, 0,
				new int[possiblePositionsOfDevices.size()], profileCopy,
				flexibleDevices, finalResultHolder);
		
		boolean changed = false;
		if (Math.abs(oldResult - finalResultHolder.res) >= Consts.epsilon) {
			changed = true;
			
			boolean oldFlexibleOps[][] = new boolean[flexibleDevices.size()][];
			for (int i = 0; i < flexibleDevices.size(); i++) {
				boolean arr[] = flexibleDevices.get(i).operations;
				oldFlexibleOps[i] = Arrays.copyOf(arr, arr.length);
			}

			updateOperationBasedOnPosition(flexibleDevices,
					finalResultHolder.config);

			boolean newFlexibleOps[][] = new boolean[flexibleDevices.size()][];
			for (int i = 0; i < flexibleDevices.size(); i++)
				newFlexibleOps[i] = flexibleDevices.get(i).operations;

			for (FlexibleDevice fd : flexibleDevices)
				fd.addComsumptionProfile(profileCopy);
			for (int i = 0; i < profile.length; i++)
				profile[i] = profileCopy[i];
		}
		
		RunnerHelper.saveIntermediateHomeProfile("After Scheduling: ", home);
		RunnerHelper.saveIntermediateSystemProfile("After Scheduling: ",home, profile);
		
		return changed;
	}

	private static void updateOperationBasedOnPosition(
			ArrayList<FlexibleDevice> flexibleDevices, int[] config) {
		for (int i = 0; i < flexibleDevices.size(); i++) {
			FlexibleDevice fd = flexibleDevices.get(i);
			fd.updateOperations(config[i]);
		}
	}

	private static void scheduleProfileHelper(
			ArrayList<ArrayList<Integer>> possiblePositionsOfDevices,
			int currentHandlingDeviceIdx, int[] results,
			double currentProfileToMinimize[],
			ArrayList<FlexibleDevice> flexibleDevices,
			FinalResultHolder finalResultHolder) {

		if (currentHandlingDeviceIdx == results.length) {
			double thisResult;

			double profileToAccumulate[] = accumulateProfile(results,
					currentProfileToMinimize, flexibleDevices);
			thisResult = calculateResultBasedOnOptimizationGoal(profileToAccumulate);

			if ((thisResult < finalResultHolder.res)
					&& ((finalResultHolder.res - thisResult) > Consts.epsilon)) {
				finalResultHolder.config = Arrays.copyOf(results,
						results.length);
				finalResultHolder.res = thisResult;
			}
			return;
		}

		ArrayList<Integer> possiblePositions = possiblePositionsOfDevices
				.get(currentHandlingDeviceIdx);
		if (possiblePositions.size() == 0) {
			results[currentHandlingDeviceIdx] = -1;
			scheduleProfileHelper(possiblePositionsOfDevices,
					currentHandlingDeviceIdx + 1, results,
					currentProfileToMinimize, flexibleDevices,
					finalResultHolder);
		} else {
			for (Integer possiblePosition : possiblePositions) {
				results[currentHandlingDeviceIdx] = possiblePosition;
				scheduleProfileHelper(possiblePositionsOfDevices,
						currentHandlingDeviceIdx + 1, results,
						currentProfileToMinimize, flexibleDevices,
						finalResultHolder);
			}
		}
	}

	private static double[] accumulateProfile(int[] results,
			double[] currentProfileToMinimize,
			ArrayList<FlexibleDevice> flexibleDevices) {

		double[] profileToAccumulate;
		profileToAccumulate = Arrays.copyOf(currentProfileToMinimize,
				currentProfileToMinimize.length);
		for (int i = 0; i < flexibleDevices.size(); i++) {
			FlexibleDevice di = flexibleDevices.get(i);
			di.accumulateProfile(profileToAccumulate, results[i]);
		}
		return profileToAccumulate;
	}

	private static double calculateResultBasedOnOptimizationGoal(
			double[] currentProfileToMinimize) {
		if (Consts.isToMinimizePeek) // if type is true, we minimize peak to
										// average ratio
		{
			return findPeak(currentProfileToMinimize); // value to be compared
														// in search process
		} else // of type is false, we minimize variance
		{
			return findVariance(currentProfileToMinimize); // value to be
															// compared in
															// search process
		}
	}

	// ********find peak power value of a power profile
	private static double findPeak(double[] profile) {
		double max = 0;
		for (int i = 0; i < profile.length; i++)
			max = Math.max(max, profile[i]);
		return max;
	}

	// ********find PAR of a power profile
	public static double findPAR(double[] profile) {
		double max = profile[0];
		double sum = 0;
		double average = 0;

		for (int i = 0; i < profile.length; i++) {
			if (profile[i] > max) {
				max = profile[i];
			}
		}
		for (int i = 0; i < profile.length; i++) {
			sum = sum + profile[i];
		}
		average = sum / (profile.length);
		max = max / average;// PAR
		return max;
	}

	// ********find the variance of a power profile
	public static double findVariance(double[] profile) {
		double variance = 0;
		double sum = 0;
		double average = 0;
		for (int i = 0; i < profile.length; i++) {
			sum = sum + profile[i];
		}
		average = sum / (profile.length);

		for (int i = 0; i < profile.length; i++) {
			variance = variance + (profile[i] - average)
					* (profile[i] - average);
		}
		variance = variance / (profile.length);
		return variance;
	}
}