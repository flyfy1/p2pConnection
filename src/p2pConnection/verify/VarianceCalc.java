package p2pConnection.verify;

import p2pConnection.schedule.Algorithm;

public class VarianceCalc {

	public static void main(String[] args) {
		double profile[] = new double[]{10.030000089999998, 9.28000006, 7.280000060000001, 7.280000060000001, 7.280000060000001, 5.280000060000001, 9.78000006, 14.43000012, 1.68000003, 0.78, 2.27999995, 2.27999995, 0.48, 2.27999995, 2.27999995, 0.48, 0.48, 5.28, 10.18000003, 5.68000003, 6.1300000599999995, 6.1300000599999995, 10.93000012, 14.43000012};
		System.out.println(Algorithm.findVariance(profile));
	}

}
