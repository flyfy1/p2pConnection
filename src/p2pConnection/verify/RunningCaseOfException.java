package p2pConnection.verify;

import java.io.IOException;

import p2pConnection.node.Home;
import p2pConnection.runner.RunnerHelper;
import p2pConnection.schedule.Algorithm;

public class RunningCaseOfException {
	public static void main(String args[]) throws IOException{
		Home homes[] = new Home[3];
		homes[0] = Home.getHome("device1");
		homes[1] = Home.getHome("device2");
		homes[2] = Home.getHome("device3");
		
		for(Home h: homes){
			h.writeIntermediateProfile("Initialization");
		}
		
		int noChangeCnt = 0;
		int homeEncounterCnt = 0;
		double profile[] = null;
		while(noChangeCnt <= 3){
			for(int i = 0 ; i < homes.length ; i++){
				profile = getHomeProfiles(homes);
				if(i == 2){
					if(homeEncounterCnt >= 0){
						continue;
					}
					homeEncounterCnt++;
				}
				if(Algorithm.scheduleProfile(homes[i], profile)){
					noChangeCnt = 0;
				} else{
					noChangeCnt++;
				}
			}
		}
		
		System.out.println("Final Result: " + RunnerHelper.getMeasurement(profile));
	}
	
	private static double[] getHomeProfiles(Home[] homes) {
		double profile[] = new double[24];
		accumulateProfile(profile, homes[0].getHomeProfile(false));
		accumulateProfile(profile, homes[1].getHomeProfile(false));
		accumulateProfile(profile, homes[2].getHomeProfile(false));
		return profile;
	}
	
	
	
	public static void accumulateProfile(double result[],double homeProfile[]) {
		for(int i = 0 ; i < homeProfile.length ; i++){
			result[i] += homeProfile[i];
		}
	}
}
