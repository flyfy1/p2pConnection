package p2pConnection.verify;

import java.util.LinkedList;

import p2pConnection.node.Home;

public class VerifyRandom {

	public static void main(String[] args) {
		String serverNameBase = "device";
		int expectedNumberOfDevices = 3;
		int deviceCnt = 1;
		LinkedList<Home> homes = new LinkedList<Home>();
		
		while(deviceCnt <= expectedNumberOfDevices){
			Home h = Home.getHome(serverNameBase + deviceCnt);
			System.out.println("initialize "+ deviceCnt + " ");
			h.outputProfile();
			homes.add(h);
			deviceCnt++;
		}

	}

}
