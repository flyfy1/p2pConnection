package p2pConnection.runner;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import p2pConnection.node.DeviceInfo;
import p2pConnection.node.Home;
import p2pConnection.runner.WebAPI.WebAPIDeviceInfoContainer;
import p2pConnection.schedule.NodeSchedulingThread;

public class SingleHomeRunner extends Thread {
	Home home;
	Callback callback;

	public static interface Callback {
		public void done(double finalProfile[], Home home);
	}

	public SingleHomeRunner(Home h, Callback callback) {
		this.home = h;
		this.callback = callback;
	}

	@Override
	public void run() {
		// Wait until home is ready to start
		List<DeviceInfo> deviceInfos;
		long previousTime = new Date().getTime(), currentTime;
		WebAPIDeviceInfoContainer webInfo;

		boolean timeoutHasReached = false;
		boolean tryOneMoreTime = true;

		do {
			webInfo = WebAPI.getDeviceInfo();
			currentTime = new Date().getTime();

			if (webInfo.expectedNum <= webInfo.infoList.size()) {
				break;
			}

			if (currentTime - previousTime > home.myTimeout * 1000) {
				System.out.println("Time-out has reached");
				timeoutHasReached = true;
				break;
			}

			try {
				TimeUnit.MICROSECONDS.sleep(100);
			} catch (InterruptedException e) {
			}

			if (home.schedulingIsStarted) {
				if (tryOneMoreTime)
					tryOneMoreTime = false;
				else
					break;
			}
		} while (true);

		deviceInfos = webInfo.infoList;
		home.setDeviceInfos(webInfo.infoList);

		// Do nothing if the scheduling has been started by other nodes
		if (home.schedulingIsStarted)
			return;

		// Start this scheduling process... formally
		DeviceInfo startingDevice = deviceInfos.get(0);
		if (timeoutHasReached || startingDevice.id == home.myId) {
			startSchedulingThread();
		}
	}

	private void startSchedulingThread() {
		NodeSchedulingThread schedulingThread = new NodeSchedulingThread(home,
				0, callback);
		schedulingThread.start();
		System.out.println("Scheduling Thread Started");
	}
}
