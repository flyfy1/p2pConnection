package p2pConnection.runner;

import java.io.IOException;

import p2pConnection.node.Home;


public class IndividualRun {
	
	public static void main(String args[]) throws IOException{
		//determine what is standard of optimization
		String nodeName = "device2";
		
		Home h = Home.getHome(nodeName);	h.serve();
		
		SingleHomeRunner homeRunner = new SingleHomeRunner(h, IntegratedRun.callback);
		homeRunner.start();
	}
}
