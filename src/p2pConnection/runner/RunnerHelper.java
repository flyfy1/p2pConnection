package p2pConnection.runner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import p2pConnection.Consts;
import p2pConnection.node.Home;
import p2pConnection.schedule.Algorithm;

public class RunnerHelper {
	public static void outputSystemProfile(String prefix,double[] profile) {
		double typeResultForSystem = RunnerHelper.getMeasurement(profile);
		System.out.printf("%s system's power profile: %s\n",prefix, Arrays.toString(profile));
		System.out.printf("%s System's %s profile: %.3f\n", prefix, getProfileType(), typeResultForSystem);
	}
	
	public static void outputHomeProfile(String prefix,double[] profile, Home home){
		double typeResultForHome = RunnerHelper.getMeasurement(home.getHomeProfile(false));
		System.out.printf("%s %s result of home %s: %.3f\n", prefix, getProfileType(), home.myName, typeResultForHome);
	}

	private static String getProfileType() {
		String profileType;
		if(Consts.isToMinimizePeek){
			profileType = "PAR";
		} else{
			profileType = "Variance";
		}
		return profileType;
	}
	
	public static double getMeasurement(double[] powerProfile) {
		double result;
		if(Consts.isToMinimizePeek) {
			result = Algorithm.findPAR(powerProfile);
		} else {
			result = Algorithm.findVariance(powerProfile);
		}
			
		return result;
	}

	public static void saveIntermediateHomeProfile(String recordingCondition, Home home) {
		try{
			home.writeIntermediateProfile(recordingCondition);
		}catch(IOException e){}
	}

	public static void saveIntermediateSystemProfile(
			String recordingCondition, 	Home home, double[] profile) {
		try{
			csvWritter.write(recordingCondition + "(" + home.myName + ") of system profile: ");
			csvWritter.write(Arrays.toString(profile));
			csvWritter.write('\n');
			csvWritter.write("Calculated " + getProfileType() + " result: " + getMeasurement(profile));
			csvWritter.write('\n');
			csvWritter.flush();
		}catch(IOException e){}
	}
	
	public static void  shutDown() {
		try{
			System.out.println("Goign to save result");
			csvWritter.close();
			System.out.println("Profile saved");
		} catch(IOException e){}
	}
	
	private static File systemProfileFile = new File(Consts.csvFolder + Consts.systemProfileName);
	private static BufferedWriter csvWritter = null;
	static{
		try{
			csvWritter = new BufferedWriter(new FileWriter(systemProfileFile));
		} catch(IOException e){
			e.printStackTrace();
		}
	}
}
