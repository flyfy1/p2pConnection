package p2pConnection.runner;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.management.RuntimeErrorException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import p2pConnection.Consts;
import p2pConnection.node.DeviceInfo;

public class WebAPI {
	private static String connectWithParam(String postParams) throws IOException{
		
		URL obj = new URL(Consts.INFO_SERVER);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(postParams);
		wr.flush();
		wr.close();
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);response.append('\n');
		}
		in.close();
		return response.toString();
	}
	
	public static boolean start(int expectedDeviceNumber){
		try{
			String retStr = connectWithParam("action=start&device_num=" + expectedDeviceNumber);
			return retStr.startsWith("start_time set at: ");
		}catch(IOException e){
			e.printStackTrace();
			return false;
		}
	}
	
	public static class WebAPIDeviceInfoContainer{
		ArrayList<DeviceInfo> infoList;
		int expectedNum;
		
		private WebAPIDeviceInfoContainer(long deviceNum){
			this.expectedNum = (int) deviceNum;
			infoList = new ArrayList<DeviceInfo>();
		}
	}
	public static WebAPIDeviceInfoContainer getDeviceInfo(){
		WebAPIDeviceInfoContainer finalRes = null;
		try {
			String results = connectWithParam("action=query");
//			System.out.println("Result: " + results);
			JSONObject resultObj = (JSONObject)JSONValue.parse(results);
			if(resultObj == null){
				throw new RuntimeException(results);
			}
			long deviceNum = (Long) resultObj.get("device_num");
			
			finalRes = new WebAPIDeviceInfoContainer(deviceNum);
			JSONArray arr = (JSONArray)resultObj.get("devices");
			
			for(int i = 0 ; i < arr.size(); i++){
				JSONObject obj = (JSONObject) arr.get(i);
				DeviceInfo info = new DeviceInfo();
				info.id = (Long) obj.get("id");
				info.name = (String) obj.get("name");
				info.port = Integer.parseInt((String) obj.get("port"));
				info.ip = (String) obj.get("ip");
				finalRes.infoList.add(info);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return finalRes;
	}

	public static String registerHome(String myName, String myIP, int myPort) throws IOException {
		String registerContent = "action=update&name=" + myName + "&ip=" + myIP + "&port=" + myPort;
		return connectWithParam(registerContent);
	}

	public static boolean done() {
		try{
			connectWithParam("action=done");
			return true;
		} catch(IOException e){
			return false;
		}
	}
}
