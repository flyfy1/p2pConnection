package p2pConnection.runner;

import java.io.IOException;
import java.util.LinkedList;

import p2pConnection.node.Home;
import p2pConnection.runner.SingleHomeRunner.Callback;

public class IntegratedRun {
	
	public static Callback callback = new Callback() {
		@Override
		public void done(double profile[], Home home) {
			
			RunnerHelper.outputHomeProfile("Final",profile, home);
			RunnerHelper.outputSystemProfile("Final",profile);
			
			home.schedulingDone();
			
			WebAPI.done();
		}
	};

	public static void main(String[] args) throws IOException {
		/**
		 * There're several steps to fully start running the test conf.
		 * 
		 * 1. Register a new run session, and the expected number of devices
		 * 
		 * 2. Init threads to simulate the servers; if a port is occupied, take the next in the queue
		 * 
		 * 3. Register the server's name, ip, and port number
		 * 
		 * 4. Then keep pulling, until valid result got
		 * 
		 * 5. After got information of other devices', start running (from the first node)
		 * 
		 * 6. After all device done, de-register this running session
		 */
		String serverNameBase = "device";
		int expectedNumberOfDevices = 3;
		
		// step 1
		if(!WebAPI.start(3)) 	return;
		
		// step 2 & 3
		int deviceCnt = 1;
		LinkedList<Home> homes = new LinkedList<Home>();
		
		//Initialize power profile of each home
		while(deviceCnt <= expectedNumberOfDevices){
			Home h = Home.getHome(serverNameBase + deviceCnt);
			homes.add(h);
			h.serve();
			deviceCnt++;
		}
		
		// step 4, 5, 6
		LinkedList<SingleHomeRunner> homeRunners = new LinkedList<SingleHomeRunner>();
		for(Home h: homes){
			SingleHomeRunner runner = new SingleHomeRunner(h, callback);
			homeRunners.add(runner);
		}
		
		for(SingleHomeRunner runner: homeRunners){
			runner.start();
		}
	}

}
