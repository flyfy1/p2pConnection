package p2pConnection.node;


public class FlexibleDevice extends PowerDevice{
	public int hoursNeeded;
	
	public boolean constraints[] = new boolean[24];
	public boolean operations [] = new boolean[24];
	
	/**
	 * update flexible device operation profile
	 * @param start
	 */
	public void updateOperations(int start)
	{
		int hours = hoursNeeded;
		for(int i=0;i<operations.length;i++)
		{
			operations[i]=false;
		}
		while(hours!=0)
		{
			if(!(start<24))
			{
				start = start-24;
			}
			operations[start]=true;
			hours--;
			start++;
		}
	}

	@Override
	public void addComsumptionProfile(double profileToAccumulate[]) {
		double profile[] = new double[24];
		for(int i = 0 ; i < profile.length ; i++){
			if(operations[i]) profile[i] += powerRequired;
		}
	}

	public void randomize() {
		boolean possibleStartingSlots[] = possibleStartingSlots();
		int desiredSlot = (int)(Math.random() * 24) % 24;
		int ptr = 0;
		while(desiredSlot > 0){
			while(!possibleStartingSlots[ptr]){
				ptr = (ptr + 1) % 24;
			}
			
			ptr = (ptr + 1) % 24;
			desiredSlot--;
		}
		updateOperations(ptr);
		//System.out.println(Arrays.toString(operations));
	}
	
	private boolean[] possibleStartingSlots(){
		boolean possibleStartingSlots[] = new boolean[24];
		
		fromSkretch: for(int i = 0 ; i < constraints.length ; i++){
			if(!constraints[i]) continue;
			
			for(int j = 0; j < hoursNeeded; j++){
				int thisHour = (i + j) % 24;
				if(!constraints[thisHour]) continue fromSkretch;
			}
			
			possibleStartingSlots[i] = true;
		}
		
		return possibleStartingSlots;
	}

	public boolean isValidStartingTime(int hour) {
		for(int i = 0; i < hoursNeeded ; i++){
			if(!constraints[(i + hour) % 24])	return false;
		}
		return true;
	}
	
	public void accumulateProfile(double[] profileToAccumulate, int startTime) {
		for(int i = 0 ; i < hoursNeeded ; i++){
			profileToAccumulate[(startTime + i)%24] += powerRequired;
		}
	}
}