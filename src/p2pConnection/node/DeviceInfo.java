package p2pConnection.node;

public class DeviceInfo {
	public long id;
	public String name;
	public String ip;
	public Integer port;
	
	// Used for the handling of exceptions
	public boolean isLost = false;
	public String previousProfile;
	
	@Override public String toString(){
		return this.name + "(" + this.id + ") " + this.ip + ": " + this.port;
	}
}
