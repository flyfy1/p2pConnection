package p2pConnection.node;

public class FixedDevice extends PowerDevice{
	public boolean runTime[] = new boolean[24];
	
	@Override
	public void addComsumptionProfile(double[] profileToAccumulate){
		for(int i = 0 ; i < runTime.length; i++){
			if(runTime[i]) profileToAccumulate[i] += powerRequired;
		}
	}
}