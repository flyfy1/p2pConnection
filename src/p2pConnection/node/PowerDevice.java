package p2pConnection.node;

public abstract class PowerDevice {
	String name;
	public double powerRequired;
	
	public abstract void addComsumptionProfile(double[] profileToAccumulate);
}
