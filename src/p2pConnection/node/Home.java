package p2pConnection.node;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import p2pConnection.Consts;
import p2pConnection.Helper;
import p2pConnection.network.Server;
import p2pConnection.network.Server.OnClientActionListener;
import p2pConnection.runner.IntegratedRun;
import p2pConnection.runner.WebAPI;
import p2pConnection.schedule.NodeSchedulingThread;

enum ReadState{
	idle, readFixed, readFlexible
}

public class Home {
	public static boolean isFirstTimeGotProfileOverall = true;
	private Home thisHome = this;
	private static final Pattern startCommendPattern = Pattern.compile("^start,([0-9]+)$");
	
	// Important: Assume when the server listener is called, the deviceInfo is set
	private List<DeviceInfo> deviceInfos;
	
	public ArrayList<FlexibleDevice> flexibleDevices;
	public ArrayList<FixedDevice> fixedDevices;
	
	// Information about the connection
	public String myIP;
	public String myName;
	int myPort;
	Server server;
	public int myId;
	
	public void setDeviceInfos(List<DeviceInfo> devices){
		this.deviceInfos = devices;
	}
	public List<DeviceInfo> getDeviceInfos(){return this.deviceInfos;}
	
	private OnClientActionListener serverListener = new OnClientActionListener(){
		
		@Override
		public void onClientAction(String clientRequest,
				DataOutputStream outToClient) throws IOException {
			Matcher startCommandMatcher = startCommendPattern.matcher(clientRequest);
			
			if(startCommandMatcher.matches()){
				int count = Integer.parseInt(startCommandMatcher.group(1));
//				System.out.println("Node has been notified to start: " + myName);
				(new NodeSchedulingThread(thisHome,count,IntegratedRun.callback)).start();
				outToClient.writeBytes("Server Started.");
			} else if(clientRequest.equals("query")){
				outToClient.writeBytes(getHomeProfileString());
			} else{
				outToClient.writeBytes("Invalid Action Required");
			}
		}
	};
	public long myTimeout = Consts.timeoutSec;
	
	private String getHomeProfileString() {
		double profile[] = getHomeProfile(false);
		boolean first = true;
		DecimalFormat df = new DecimalFormat("#.########");
		
		StringBuilder sb = new StringBuilder();
		
		
		for(double costAtThisHour:profile){
			if(first) first = false;
			else sb.append("\t");
			
			sb.append(df.format(costAtThisHour));
		}
		
		return sb.toString();
	}
	
	public void outputProfile(){
		System.out.println(getHomeProfileString());
	}//output the total power profile of current home
	
	protected Home(String homename){
		this.myName = homename;
		flexibleDevices = new ArrayList<FlexibleDevice>();
		fixedDevices = new ArrayList<FixedDevice>();
		
		myIP = Helper.findAppropriateIp();
		myPort = Consts.serverStartingPort;
	};

	public static Home getHome(String homeName){
		return getHome(homeName, Consts.deviceFolder + "/");
	}
	public static Home getHome(String homeName, String homeFolder){
		Home h = new Home(homeName);
		
		//String deviceFileName = Consts.deviceFolder + "/" + homeName + ".txt";
		String deviceFileName = homeFolder + homeName + ".txt";
		File f = new File(deviceFileName);
		
		String line;
		try{
			BufferedReader br = new BufferedReader(new FileReader(f));
			while( (line = br.readLine()) != null){
				h.readDeviceConfig(line);
			}
		} catch(IOException e){
			return null;
		}
		
		// Randomly assign a device profile for the random device
		for(FlexibleDevice flexibleDevice: h.flexibleDevices){
			flexibleDevice.randomize();
		}
		
		do{
			h.myPort++;
			h.server = Server.getServer(h.myPort, h.serverListener);
		} while(h.server == null);
		
		try{
			h.myId = Integer.parseInt(WebAPI.registerHome(h.myName, h.myIP, h.myPort).trim());
			h.myTimeout  = Consts.timeoutSec * (h.myId % 5 + 1);
		} catch (IOException e) {
			System.out.println("Oops...");
			return null;
		}
		try {
			h.homeProfileWritter = new BufferedWriter(new FileWriter(new File(Consts.csvFolder + h.myName)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return h;
	}
	
	/**
	 * @return
	 * The Home profile, in the time span of 24 hour, per hour basis
	 */
	public double[] getHomeProfile(boolean flexibleDeviceOnly){
		double profile[] = new double[24];
		Arrays.fill(profile, 0);
		
		if(!flexibleDeviceOnly){
			for(FixedDevice d: fixedDevices){
				for(int i = 0 ; i < d.runTime.length ; i++){
					if(d.runTime[i]){
						profile[i] += d.powerRequired;
					}
				}
			}
		}
		for(FlexibleDevice d: flexibleDevices){
			for(int i = 0 ; i < d.operations.length; i++){
				if(d.operations[i]){
					profile[i] += d.powerRequired;
				}
			}
		}
		return profile;
	}
	
	public void serve() throws IOException{
		server.start();
	}
	
	private ReadState readState;
	public boolean isFirstTimeGotProfile = true;
	public boolean schedulingIsStarted = false;
	private BufferedWriter homeProfileWritter;

	public void readDeviceConfig(String line) {
		if(line.equals("Fixed:")){
			readState = ReadState.readFixed;
		} else if(line.equals("Flexible:")){
			readState = ReadState.readFlexible;
		} else{
			String runTimeTerm;
			String terms[] = line.split(" +");
			if(terms.length < 3) return;
			switch(readState){
			case readFixed:
				FixedDevice fixedDevice = new FixedDevice();
				
				fixedDevice.name = terms[2];
				fixedDevice.powerRequired = Float.parseFloat(terms[1]);
				runTimeTerm = getTermNoBracket(terms[0]);
				
				fulFileArray(runTimeTerm, fixedDevice.runTime);
				
				fixedDevices.add(fixedDevice);
				break;
			case readFlexible:
				terms = line.split(" +");
				FlexibleDevice flexibleDevice = new FlexibleDevice();
				
				flexibleDevice.name = terms[0];
				flexibleDevice.powerRequired = Float.parseFloat(terms[1]);
				flexibleDevice.hoursNeeded = Integer.parseInt(terms[2]);
				
				runTimeTerm = getTermNoBracket(terms[3]);
				fulFileArray(runTimeTerm, flexibleDevice.constraints);
				
				runTimeTerm = getTermNoBracket(terms[4]);
				fulFileArray(runTimeTerm, flexibleDevice.operations);
				
				flexibleDevices.add(flexibleDevice);
				break;
			}
		}
	}
	
	private void fulFileArray(String runTimeTerm, boolean[] constraints) {
		for(int i = 0; i < runTimeTerm.length(); i++){
			char rt = runTimeTerm.charAt(i);
			constraints[i] = (rt == '1');
		}
	}

	private String getTermNoBracket(String termWithBracket){
		if(termWithBracket == null)	return null;
		termWithBracket = termWithBracket.trim();
		if(termWithBracket.charAt(0) != '[')	return termWithBracket;
		return termWithBracket.substring(1,termWithBracket.length() - 1);
	}
	
	
	public void schedulingDone() {
		try{
			homeProfileWritter.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	public void writeIntermediateProfile(String recordingCondition) throws IOException {
		homeProfileWritter.write(recordingCondition);
		homeProfileWritter.write(getHomeProfileString());
		homeProfileWritter.write('\n');
		homeProfileWritter.flush();
	}
	
	long lastStartTime;
	private Home that = this;
	
	Thread countdownThread = new Thread(){
		@Override public void run() {
			lastStartTime = new Date().getTime();
			
			
			while(schedulingIsStarted){
				long currentTime = new Date().getTime();
				
				if(currentTime - lastStartTime > myTimeout * 1000){
					lastStartTime = currentTime;
					NodeSchedulingThread t = new NodeSchedulingThread(that , 0, IntegratedRun.callback);
					t.start();;
				}
				
				try{
					TimeUnit.MILLISECONDS.sleep(myTimeout * 100);
				} catch (InterruptedException e) {e.printStackTrace();}
			}
		};
	};
	
	public void setSchedulingHasStarted() {
		this.lastStartTime = new Date().getTime();
		if(schedulingIsStarted) return;
		
		this.schedulingIsStarted = true;
		countdownThread.start();
	}
}