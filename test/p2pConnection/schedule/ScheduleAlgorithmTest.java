package p2pConnection.schedule;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import p2pConnection.node.FixedDevice;
import p2pConnection.node.FlexibleDevice;
import p2pConnection.node.Home;

public class ScheduleAlgorithmTest {
	
	private Home home;
	private Home home_2;
	private Home home_3;
	private double powerProfiles[];
	
	@Test
	public void integrationTest2() {
		home = Home.getHome("device1_1","test/testFiles/devices/");
		home_2 = Home.getHome("device2_1","test/testFiles/devices/");
		home_3 = Home.getHome("device3_1","test/testFiles/devices/");
		
		powerProfiles = new double[24];
		//System.out.println(Arrays.toString(powerProfile));
		generateProfile(home_2,powerProfiles);
		generateProfile(home_3,powerProfiles);
		generateProfile(home,powerProfiles);
		
		System.out.println("Test2"+"Before Schedule: " + Arrays.toString(powerProfiles));
		boolean changed = Algorithm.scheduleProfile(home_3,powerProfiles);
		System.out.println("Test2"+"After Schedule: " + Arrays.toString(powerProfiles));
		Assert.assertEquals(false, changed);
	}

	private void generateProfile(Home homeinfo, double[] profile) {
		int i=0;
		for(FixedDevice d: homeinfo.fixedDevices){
			for(i=0;i<d.runTime.length;i++)
			{
				if(d.runTime[i])
				{
					profile[i]=profile[i]+d.powerRequired;
				}
			}
		}
		for(FlexibleDevice f: homeinfo.flexibleDevices){
			for(i=0;i<f.operations.length;i++)
			{
				if(f.operations[i])
				{
					profile[i]=profile[i]+f.powerRequired;
				}
			}
		}
		return;
	}
	
}
