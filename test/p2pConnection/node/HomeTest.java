package p2pConnection.node;

import static p2pConnection.Consts.deviceFolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

public class HomeTest {

	@Test
	public void test() throws IOException {
		File deviceFiles = new File(deviceFolder);
		
		for(File f:deviceFiles.listFiles()){
			BufferedReader br = new BufferedReader(new FileReader(f));
			Home h = new Home("HomeName");
			
			String line;
			while( (line = br.readLine()) != null){
				h.readDeviceConfig(line);
			}
		}
	}

}
